/* 
1. Do you prefer vuejs or reactjs? Why?
ReactJs because most companies use reactJs, the opportunity to work in the world of FrontEnd is greater

2. What complex things have you done in frontend development?
when using life circle react in the component class we can't use several functions like componentWillMount, componentWillUnMount, etc, we have to play the conditions inside componentDidMount

3. Why do UI Developers need to know and understand UX? how far do you understand it?
yes because without the user experiment (UX) we will not know what kind of UI that many users like, and for an attractive and simple appearance requires UX

4. Provide the results of your analysis regarding https://taskdev.mile.app/login from the UI / UX side!
5. Create a better login page based on https://taskdev.mile.app/login and apply it on https://www.netlify.com (https://www.netlify.com/)!
6. Solve the logic problems below!
*/

/*  6.a */
var A = 3
var B = 5

A = B - A 
B = B - A
A = B + A

console.log('6.a');
console.log('A',A);
console.log('B',B);
console.log('-----------------------------');
/*  6.b */
var numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
var total = numbers.length
setTimeout(() => {
    total = numbers.length + missing.length
}, 100);

count = total;
var missing = new Array();

for (var i = 1; i <= count; i++) {
  if (numbers.indexOf(i) == -1) {
    missing.push(i);
  }
}
console.log('6.b');
console.log(missing);
console.log('---------------------------');
/*  6.c */
arrayNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
var duplicate = [];
var unique = [];
for (const num of arrayNumbers) {
  if (unique.includes(num)) {
    duplicate.push(num)
    
    continue;
  }
  unique.push(num)
  
}
const result = []
const dataDouble = []
for (const d of duplicate) {
  if (result.includes(d)) {
    dataDouble.push(d)
    continue;
  }
  result.push(d)
  
}
console.log('6.c');
console.log(result);


